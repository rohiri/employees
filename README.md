# EMPLOYEES APP


## Requerimientos

- Cliente de Docker instalado localmente

## Paso a Paso

1. Clonar el Repositorio

`git clone https://gitlab.com/rohiri/employees.git`

2.  Ejecutar los contenedores

`docker-compose up --build`

## Nota
Si presenta algun error porfavor instale las dependencias de npm en frontend y en backend.

Ir a la carpeta frontend y ejecutar

`npm install`

Luego reintentar el paso 2


