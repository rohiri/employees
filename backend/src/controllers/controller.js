const { Pool } = require('pg');

const pool = new Pool({
    user: 'postgres',
    host: 'postgresql',
    password: 'postgres',
    database: 'employee',
    port: '5432'
});

const getEmployees = async (req, res) => {
    const response = await pool.query("SELECT rl1.*, rl1.function as function_name, rl2.fullname as boss_name FROM employees rl1 LEFT JOIN employees rl2 ON rl2.id = rl1.boss ORDER BY id ASC");
    res.status(200).json(response.rows);
};

const getEmployeeById = async (req, res) => {
    const id = parseInt(req.params.id);

    const response = await pool.query('SELECT * FROM employees WHERE id = $1', [id]);

    if (response.rowCount == 0) {
        res.status(404).json("Employee Not Found");
    }

    res.json({
        id: response.rows[0].id,
        fullname: response.rows[0].fullname,
        function_name: response.rows[0].function,
        boss: response.rows[0].boss
    });
};

const createEmployee = async (req, res) => {
    const { fullname, function_name, boss } = req.body;


    const checkresponse = await pool.query('SELECT * FROM employees WHERE fullname = $1', [fullname]);

    if (checkresponse.rowCount !== 0) {
        return res.status(409).json({
            status: "Error",
            message: "Employee Already Exist (Invalid Payload)"
        });
    }

    const response = await pool.query('INSERT INTO employees (fullname, function, boss) VALUES ($1, $2, $3)', [fullname, function_name, boss]);

    if (fullname && function_name) {
        return res.status(201).json({
            message: 'Employee Added successfully',
            body: {
                employee: {fullname, function_name, boss}
            }
        })
    }

    res.status(400).json({
        status: "Error",
        message: "Employee Not Created (Invalid Payload)"
    })
};

const updateEmployee = async (req, res) => {
    const id = parseInt(req.params.id);
    const { fullname, function_name, boss } = req.body;

    const response =await pool.query('UPDATE employees SET fullname = $1, function = $2, boss = $3 WHERE id = $4', [
        fullname,
        function_name,
        boss,
        id
    ]);
    res.json('Employee Updated Successfully');
};

const deleteEmployee = async (req, res) => {
    const id = parseInt(req.params.id);
    await pool.query('DELETE FROM employees where id = $1', [
        id
    ]);
    res.json(`Employee ${id} deleted Successfully`);
};

module.exports = {
    getEmployees,
    getEmployeeById,
    createEmployee,
    updateEmployee,
    deleteEmployee
};