const request = require("supertest");

const app = require("../src/index");

/**
 * Testing get all employees endpoint
 */
describe("GET /employees", () => {
  it("Respond with json containing a list of all employees", (done) => {
    request(app)
      .get("/employees")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});

/**
 * Testing POST Employee endpoint
 */
describe("POST /employees", () => {
  it("respond with 201 created", (done) => {
    const data = {
      fullname: "Faker New Employee",
      function_name: "Developer",
      boss: null
    };
    request(app)
      .post("/employees")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(201)
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });

  it("Invalid payload respond with 400 on bad request", (done) => {
    const data = {};
    request(app)
      .post("/employees")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(400)
      .expect('{"status":"Error","message":"Employee Not Created (Invalid Payload)"}')
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });

  it("respond with 409 when employee exist", (done) => {
    const data = {
      fullname: "Faker Employee",
      function_name: "Developer",
      boss: null
    };
    request(app)
      .post("/employees")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(409)
      .expect('{"status":"Error","message":"Employee Already Exist (Invalid Payload)"}')
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });

  /**
   * Testing employee/:id endpoint by giving an existing employee
   */
  describe("GET /employees/:id", () => {
    it("respond with json containing a single employee", (done) => {
      request(app)
        .get("/employees/1")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });

    it("respond with json employee not found when the employee not exists", (done) => {
      request(app)
        .get("/employees/100")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(404)
        .expect('"Employee Not Found"')
        .end((err) => {
          if (err) return done(err);
          done();
        });
    });
  });
});