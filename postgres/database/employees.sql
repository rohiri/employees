CREATE TABLE employees (
    id SERIAL PRIMARY KEY,
    fullname VARCHAR(100),
    function VARCHAR(100),
    boss INTEGER DEFAULT NULL
);

INSERT INTO employees (fullname, function, boss)
    VALUES ('William Torres', 'CEO', null),
    ('Jhon Doe', 'developer', 1);