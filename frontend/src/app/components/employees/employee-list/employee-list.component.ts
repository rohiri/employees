import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee';
import { EmployeeService } from 'src/app/services/employee.service';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees?: Employee[];

  currentEmployee: Employee = {
    fullname: '',
    function_name: '',
    boss: ''
  };

  constructor(
    private employeeService: EmployeeService
  ) { }

  ngOnInit(): void {
    this.retrieveEmployees();
  }

  retrieveEmployees(): void {
    this.employeeService.getAll()
      .subscribe(
        data => {
          this.employees = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  onDelete(id: any) {
    Swal.fire('Hello world!')
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this employee',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.employeeService.delete(id)
          .subscribe(
            response => {
              console.log(response);
              this.retrieveEmployees();
            },
            error => {
              console.log(error);
            });
        Swal.fire(
          'Deleted!',
          'Your Employee has been deleted.',
          'success'
        )
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your Employee file is safe :)',
          'error'
        )
      }
    })
  }



  onEdit(employee: Employee) {
    
    this.employeeService.selectedEmployee = Object.assign({}, employee)
    // this.employeeService.update(employee)
    //   .subscribe(
    //     response => {
    //       console.log(response);
    //       this.retrieveEmployees();
    //     },
    //     error => {
    //       console.log(error);
    //     });
  }



}