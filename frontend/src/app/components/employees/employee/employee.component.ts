import { Component, OnInit } from '@angular/core';
import { NgForm, ɵNgNoValidate } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2'

//  Service 
import { EmployeeService } from '../../../services/employee.service';

// Class
import { Employee } from '../../../models/employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(
    public employeeService: EmployeeService
  ) { }

  ngOnInit(): void {
  }

  onSubmit(employeeForm: NgForm)
  {
    if(employeeForm.value.id == null){
      this.employeeService.create(employeeForm.value)
        .subscribe(
        response => {
          Swal.fire(response.message)
        },
        error => {
          Swal.fire(error.error.message)
          console.log(error);
        });;
    }
    else{
      this.employeeService.update(employeeForm.value)
      .subscribe(
        response => {
          Swal.fire(response)
        },
        error => {
          Swal.fire(error.error.message)
          console.log(error);
        });
    }

    this.resetForm(employeeForm);
  }

  resetForm(employeeForm?: NgForm)
  {
    if(employeeForm != null)
      employeeForm.reset();
      this.employeeService.selectedEmployee = new Employee();
  }

}
