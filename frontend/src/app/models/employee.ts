export class Employee {
    id?: number;
    fullname?: string;
    function_name?: string;
    boss?: string;
    boss_name?: string;
}