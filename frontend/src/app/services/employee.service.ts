import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee'

const baseUrl = 'http://localhost:3000/employees';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  selectedEmployee: Employee = new Employee();

  constructor(private http: HttpClient) { }

  getAll(): Observable<Employee[]> {
    return this.http.get<Employee[]>(baseUrl);
  }

  get(employee : Employee): Observable<Employee> {
    return this.http.get(`${baseUrl}/${employee.id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${data.id}`, data);
  }

  delete(id: number) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

}